<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Blade
{

    public function __construct()
    {
        $path = [
            APPPATH . 'views/'
        ];
        $cachePath = APPPATH . 'cache/views'; // cấu hình thư mục cache của tập tin
        
        $compiler = new \Blade\Compilers\BladeCompiler($cachePath);
        $engine = new \Blade\Engines\CompilerEngine($compiler);
        $finder = new \Blade\FileViewFinder($path);
        
        // Nếu bạn cần thêm một phần mở rộng tập tin tùy chỉnh, hãy sử dụng phương pháp sau
        $finder->addExtension('php');
        $this->factory = new \Blade\Factory($engine, $finder);
    }

    public function view($path, $vars = [])
    {
        echo $this->factory->make($path, $vars);
    }

    public function exists($path)
    {
        return $this->factory->exists($path);
    }

    public function share($key, $value)
    {
        return $this->factory->share($key, $value);
    }

    public function render($path, $vars = [])
    {
        return $this->factory->make($path, $vars)->render();
    }
}
